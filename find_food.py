"""
A script to ask SeeFood if it sees food in the image at 
path specified by the command line argument.

For CEG 4110 by: Derek Doran
contact: derek.doran@wright.edu
Aug 9 2017
"""
import argparse
import numpy as np
import tensorflow as tf
from PIL import Image, ImageFont, ImageDraw
import os


class JianYang:
    def __init__(self):
        self.image = None
        self.modified_image = None
        self.status = None
        self.image_scores = None

        # Tensor stuff
        self.sess, self.saver, self.graph, self.x_input, self.keep_prob, self.class_scores = self.get_tensor()

    def check_image(self):
        """
        Validates the image and makes sure it's valid.
            An invalid image would be:
                1) A different file (.pdf/.txt) which just has been 'renamed' to <file_name>.png
                2) A 'broken' image; a corrupt image
        :return: Valid - a bool if the image is valid(True) or not valid(False)
        """
        valid_extensions = ['.jpg', '.png', '.bmp', '.gif']  # do we support gifs?

        filename, extension = os.path.splitext(self.image)
        if extension.lower() not in valid_extensions:  # quick check; if the extension is not a .jpg/png/bmp; invalid
            return False
        try:
            Image.open(self.image)
        except IOError:
            return False

        return True  # since we're already opening the image with PIL, would save time/mem/cpu to return the Image obj

    @staticmethod
    def get_tensor():
        #  Initialization code - we only need to run this once and keep in memory.
        sess = tf.Session()
        saver = tf.train.import_meta_graph('saved_model/model_epoch5.ckpt.meta')
        saver.restore(sess, tf.train.latest_checkpoint('saved_model/'))
        graph = tf.get_default_graph()
        x_input = graph.get_tensor_by_name('Input_xn/Placeholder:0')
        keep_prob = graph.get_tensor_by_name('Placeholder:0')
        class_scores = graph.get_tensor_by_name("fc8/fc8:0")

        return sess, saver, graph, x_input, keep_prob, class_scores

    def edit_image(self):
        """
        Experimental test to try writing on the image; if the AI finds food.
        Trying to look something like
        https://s.aolcdn.com/hss/storage/midas/844edd48b5a56488c2feec9889608fa2/205268391/nothotdog.jpg is the goal
        :return:
        """
        img = Image.open(self.image)
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype("/usr/share/fonts/dejavu/DejaVuSans.ttf", 55)
        if self.status == 'No Food':  # need a boolean or something
            draw.text((0, 0), "NO FOOD", (255, 0, 0), font=font)  # RED
        elif self.status == 'Food in the image!':
            draw.text((0, 0), "FOUND FOOD", (36, 255, 50), font=font)
        else:
            print("Food hasn't been tested yet?")

        image_name, image_extension = os.path.splitext(self.image)
        new_image = image_name + "_modified" + image_extension

        print("this is the save path!", new_image)
        img.save(new_image)
        self.modified_image = new_image



    def run(self):
        """
        Doran's code to test if the passed image contains food or not.
        :return:
        """
        # The script assumes the args are perfect, this will crash and burn otherwise.

        # Work in RGBA space (A=alpha) since png's come in as RGBA, jpeg come in as RGB
        # so convert everything to RGBA and then to RGB.
        image_path = self.image
        image = Image.open(image_path).convert('RGB')
        image = image.resize((227, 227), Image.BILINEAR)
        img_tensor = [np.asarray(image, dtype=np.float32)]
        print('looking for food in ' + image_path)

        #  Run the image in the model.
        scores = self.sess.run(self.class_scores, {self.x_input: img_tensor, self.keep_prob: 1.})
        self.image_scores = scores
        print(scores)
        # if np.argmax = 0; then the first class_score was higher, e.g., the model sees food.
        # if np.argmax = 1; then the second class_score was higher, e.g., the model does not see food.
        if np.argmax(scores) == 1:
            print("No food here... :( ")
            self.status = "No Food"

        else:
            print("Oh yes... I see food! :D")
            self.status = "Food in the image!"

        return self.status, self.image_scores
