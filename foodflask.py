
import os
from flask import Flask, request, redirect, url_for, flash, send_from_directory, render_template
from werkzeug.utils import secure_filename
import requests
import urllib
import sqlite3
import math
import ast
from find_food import JianYang


UPLOAD_FOLDER = 'samples/' #  this *should* be a relative path.
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif']

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def get_ip():
    """ This is super bad. Very hard code-y and not all robust. Sorry :(
    The reason for this is how the images are refrenced, for localhost it uses 127.0.0.1:5000,
    and then we use the ip of our Amazon EC2 instance
    """
    import urllib2
    ret = urllib2.urlopen('https://enabledns.com/ip')
    ip = ret.read()
    if ip == '52.20.187.220':
        return ip
    return '127.0.0.1:5000'

ip = get_ip()


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def calculate_confidence(scores):
    """
    Will determine the confidence of the AI with it's Food-Or-NotFood selection
    :param scores: the numpy array
    :return: a string representing the confidence as a percentage
    """
    food, not_food = scores.tolist()[0]
    confidence = (1 / (1 + math.e ** (-1 * (abs(food-not_food))))) * 100
    return '{:0.3f}%'.format(confidence)


@app.route('/gallery')
def gallery():
    return render_template("gallery.html", rows=query())


@app.route('/gallery/food')
def gallery_food():
    return render_template("gallery.html", rows=[item for item in query() if item[1] == 'Food in the image!'])


@app.route('/gallery/not_food')
def gallery_not_food():
    return render_template("gallery.html", rows=[item for item in query() if item[1] == 'No Food'])


def query():
    """
    Will query the database to retrieve all the images
    :return:
    """
    con = sqlite3.connect("images.db")
    con.row_factory = sqlite3.Row

    cur = con.cursor()
    cur.execute("select * from images")

    rows = cur.fetchall()

    return rows




@app.route('/show/<filename>')
def uploaded_file(filename):
    file_name = 'http://{}/uploads/'.format(ip) + filename

    uploaded_image = os.path.join(UPLOAD_FOLDER, filename)
    jianyang.image = uploaded_image
    status, score = jianyang.run()
    confidence = calculate_confidence(score)

    database_images = [image_path[0] for image_path in query()]

    if file_name not in database_images:  # if image is NOT already in database
        with sqlite3.connect('images.db') as con:  # connect to the db, and insert the new photo information
            cur = con.cursor()
            cur.execute("INSERT INTO images(image_path,image_status,confidence) VALUES(?, ?, ?)", (file_name, status, confidence))
            con.commit()

        con.close()
    else:
        print("Notice: Image path is already in the database")
    data = {'confidence': confidence, 'status': status}
    food_boolean = status == 'Food in the image!'

    return render_template('modified.html', filename=file_name, data=data, food_boolean=food_boolean)


@app.route('/show_multiple/<files>')
def uploaded_files(files):

    list_of_files = ast.literal_eval(str(files))

    database_images = [image_path[0] for image_path in query()]
    list_of_data = []
    for file in list_of_files:
        file_name = 'http://{}/uploads/'.format(ip) + file

        uploaded_image = os.path.join(UPLOAD_FOLDER, file)
        print uploaded_image

        jianyang.image = uploaded_image
        status, score = jianyang.run()
        confidence = calculate_confidence(score)

        if file_name not in database_images:  # if image is NOT already in database
            with sqlite3.connect('images.db') as con:  # connect to the db, and insert the new photo information
                cur = con.cursor()
                cur.execute("INSERT INTO images(image_path,image_status,confidence) VALUES(?, ?, ?)",
                            (file_name, status, confidence))
                con.commit()

            con.close()
        else:
            print("Notice: Image path is already in the database")

        #data = {'confidence': confidence, 'status': status}
        food_boolean = status == 'Food in the image!'

        list_of_data.append({'image_path': file_name, 'confidence': confidence, 'status': status,
                             'food_boolean': food_boolean})

    return render_template('modified_multiple.html', data=list_of_data)



    #
    #     uploaded_image = os.path.join(UPLOAD_FOLDER, file)
    #     jianyang.image = uploaded_image
    #     status, score = jianyang.run()
    #     confidence = calculate_confidence(score)
    #
    #     database_images = [image_path[0] for image_path in query()]
    #
    #     if file_name not in database_images:  # if image is NOT already in database
    #         with sqlite3.connect('images.db') as con:  # connect to the db, and insert the new photo information
    #             cur = con.cursor()
    #             cur.execute("INSERT INTO images(image_path,image_status,confidence) VALUES(?, ?, ?)", (file_name, status, confidence))
    #             con.commit()
    #
    #         con.close()
    #     else:
    #         print("Notice: Image path is already in the database")
    #     data = {'confidence': confidence, 'status': status}
    #     food_boolean = status == 'Food in the image!'
    #
    #
    #
    # return render_template('modified.html', filename=file_name, data=data, food_boolean=food_boolean)






@app.route('/uploads/<filename>')
def send_file(filename):
    """
    Function that is called when a file is uploaded to the site from root.
    It gets the uploaded image that is saved in UPLOAD_FOLDER
    Sets the image with the JianYang class, and then checks if food is in that image with JianYang.run()


    :param filename: The filename of the image
    :return: modified.html - a page with the status of if food is in the image.
    """
    # todo  Get modified.html working.

    #uploaded_image = os.path.join(UPLOAD_FOLDER, filename)
    #jianyang.image = uploaded_image
    #jianyang.run()
    #print("test")
    return send_from_directory(UPLOAD_FOLDER, filename)


@app.route('/url/<path:url>')
def url_upload(url):
    """
    Allows a URL(needs to be a direct link to an image file) to be tested for food content.
    Steps:
        - Checks if the URL is valid (if status_code == 200)
        - Saves the image to the samples directory
        - Sets the jianyang object image to that saved image
        - checks the image
        - returns to the user if food was present or not

    :param url: The URL to the image to check
    :return: JianYang.status
    """
    # todo - Check if the url is a direct link to an image
    # todo - save the image in a unique way
    # todo - return better information; call modified.html from this method(once modified.html is working)

    status = requests.get(url).status_code
    if status == 200:  # might need to do more validation on this
        print("Valid URL")
        urllib.urlretrieve(url, 'samples/test.jpg')  # need to give each image an unique name
        jianyang.image = 'samples/test.jpg'
        jianyang.run()
        return jianyang.status
    else:
        print("Invalid URL, status code:", status)
        return "Invalid URL:" + url


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    uploaded_files = request.files.getlist("file")

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)

        if len(uploaded_files) == 0:
            print "No file was uploaded"

        if len(uploaded_files) == 1:
            file = request.files['file']
            # if user does not select file, browser also
            # submit a empty part without filename
            if file.filename == '':
                flash('No selected file')
                return redirect(request.url)
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                return redirect(url_for('uploaded_file',
                                        filename=filename))
        elif len(uploaded_files) > 1:
            file_names = []
            for f in uploaded_files:
                if allowed_file(f.filename):
                    filename = secure_filename(f.filename)
                    f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                    file_names.append(filename)


            return redirect(url_for('uploaded_files',files=file_names))


    return render_template('index.html')


if __name__ == "__main__":
    jianyang = JianYang()
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'

    app.run(debug=False)  # set debug=False (or just remove that KWA) to stop tensorflow from running twice

